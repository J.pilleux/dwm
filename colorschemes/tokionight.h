#ifndef _TOKIONIGHT_H_
#define _TOKIONIGHT_H_

static const char *fonts[]          = { "UbuntuMono Nerd Font:size=9:antialias=true" };
static const char dmenufont[]       = "UbuntuMono Nerd Font:size=9:antialias=true";
static const char col_gray1[]       = "#1a1b26";  // Dark background
static const char col_gray2[]       = "#414868";  // Completion in dmenu
static const char col_gray3[]       = "#c0caf5";  // Font color of un-focused workspace
static const char col_gray4[]       = "#15161e";  // Font color of focused workspace ( on green )
static const char col_cyan[]        = "#9ece6a";  // Green color at the top
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

#endif // _TOKIONIGHT_H_
