/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

// Colorscheme imports
#include "colorschemes/tokionight.h"

/* appearance */
static const unsigned int borderpx = 1; /* border pixel of windows */
static const unsigned int snap = 32;    /* snap pixel */
static const int showbar = 1;           /* 0 means no bar */
static const int topbar = 1;            /* 0 means bottom bar */

/* tagging */
static const char *tags[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class      instance    title       tags mask     isfloating   monitor */
    {"Gimp", NULL, NULL, 0, 1, -1},
    {"Firefox", NULL, NULL, 1 << 1, 0, -1},
};

/* layout(s) */
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints =
    1; /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen =
    1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
    /* symbol     arrange function */
    {"[]=", tile}, /* first entry is default */
    {"><>", NULL}, /* no layout function means floating behavior */
    {"[M]", monocle},
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY, TAG)                                                      \
  {MODKEY, KEY, view, {.ui = 1 << TAG}},                                       \
      {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},               \
      {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                        \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

#define BRIGHTUP XF86XK_MonBroghtnessUp
#define BRIGHTDOWN XF86XK_MonBrightnessDown

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                                             \
  {                                                                            \
    .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL }                       \
  }

/* commands */
static char dmenumon[2] =
    "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {
    "dmenu_run", "-m",      dmenumon, "-fn",    dmenufont, "-nb",     col_gray1,
    "-nf",       col_gray3, "-sb",    col_cyan, "-sf",     col_gray4, NULL};
static const char *termcmd[] = {"alacritty", NULL};
static const char *bright_up[] = {"xbacklight", "-inc", "10", NULL};
static const char *bright_down[] = {"xbacklight", "-dec", "10", NULL};

static Key keys[] = {
    /* modifier                     key        function        argument */
    {MODKEY            , XK_p, spawn, {.v = dmenucmd}},
    {MODKEY            , XK_Return, spawn, {.v = termcmd}},
    {MODKEY            , XK_b, togglebar, {0}},
    {MODKEY            , XK_t, focusstack, {.i = +1}},
    {MODKEY            , XK_s, focusstack, {.i = -1}},
    {MODKEY            , XK_i, incnmaster, {.i = +1}},
    {MODKEY            , XK_d, incnmaster, {.i = -1}},
    {MODKEY            , XK_c, setmfact, {.f = -0.05}},
    {MODKEY            , XK_r, setmfact, {.f = +0.05}},
    {MODKEY | ShiftMask, XK_Return, zoom, {0}},
    {MODKEY            , XK_Tab, view, {0}},
    {MODKEY | ShiftMask, XK_c, killclient, {0}},
    {MODKEY            , XK_t, setlayout, {.v = &layouts[0]}},
    {MODKEY            , XK_f, setlayout, {.v = &layouts[1]}},
    {MODKEY            , XK_m, setlayout, {.v = &layouts[2]}},
    {MODKEY            , XK_space, setlayout, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
    {MODKEY            , XK_agrave, view, {.ui = ~0}},
    {MODKEY | ShiftMask, XK_agrave, tag, {.ui = ~0}},
    {MODKEY            , XK_comma, focusmon, {.i = -1}},
    {MODKEY            , XK_semicolon, focusmon, {.i = +1}},
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
    {MODKEY | ShiftMask, XK_semicolon, tagmon, {.i = +1}},
    {0, XF86XK_MonBrightnessUp, spawn, {.v = bright_up}},
    {0, XF86XK_MonBrightnessDown, spawn, {.v = bright_down}},
	TAGKEYS(                        0x22,                      0)
 	TAGKEYS(                        0xab,                      1)
 	TAGKEYS(                        0xbb,                      2)
 	TAGKEYS(                        0x28,                      3)
 	TAGKEYS(                        0x29,                      4)
 	TAGKEYS(                        0x40,                      5)
 	TAGKEYS(                        0x2b,                      6)
 	TAGKEYS(                        0x2d,                      7)
 	TAGKEYS(                        0x2f,                      8)
    {MODKEY | ShiftMask, XK_q, quit, {0}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
 * ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function argument
     */
    {ClkLtSymbol, 0, Button1, setlayout, {0}},
    {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
